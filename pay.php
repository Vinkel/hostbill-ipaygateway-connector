<?php
/*
 * VOGUEPAY Payment Gateway Integration Module for HostBill
 * Author - Tes Sal
 * Email - tescointsite@gmail.com
 *
 * http://jicommit.com
 */
class ipay extends PaymentModule {

    /*
     * const NAME
     * Note: This needs to reflect class name - case sensitive.
     */
    const NAME = 'ipay';
    /*
     * const VER
     * Insert your module version here
     */
    const VER ='1.1';

    /*
     * protected $modname
     * AKA. "Nice name" - you can additionally add this variable - its contents will be displayed as module name after activation
     */
    protected $modname = 'iPay';

    /*
     * protected $description
     * If you want, you can add description to module, so its potential users will know what its for.
     */
    protected $description='iPay Payment Gateway Module.';
    /*
     * protected $filename
     * This needs to reflect actual filename of module - case sensitive.
     */
    protected $filename='class.ipay.php';

    /*
     * protected $supportedCurrencies
     * List of currencies supported by VoguePay.
     */
    protected $supportedCurrencies = array('KEN');

    /*
     * protected $configuration
     * Configuration Array
     */
    protected $configuration = array(
        'FriendlyName' => array(
            'Type' => 'System',
            'Value' => 'iPay Africa',
        ),
        // a text field type allows for single line text input
        'accountID' => array(
            'FriendlyName' => 'Account ID',
            'Type' => 'text',
            'Size' => '25',
            'Default' => 'demo',
            'Description' => 'Enter your vendor ID here',
        ),
        // a password field type allows for masked text input
        'hashkey' => array(
            'FriendlyName' => 'Secret Key',
            'Type' => 'password',
            'Size' => '25',
            'Default' => '',
            'Description' => 'Enter secret key here',
        ),
        // the yesno field type displays a single checkbox option
        'testMode' => array(
            'FriendlyName' => 'Test Mode',
            'Type' => 'dropdown',
            'Options'=> array(
                '0'=> '0',
                '1' => '1'
            ),
            'Description' => 'Tick to enable test mode 1 for Live and 0 for demo',
        ),
        // the dropdown field type renders a select menu of options
        'currency' => array(
            'FriendlyName' => 'Currency',
            'Type' => 'dropdown',
            'Options' => array(
                'KES' => 'KES',
                'USD' => 'USD'

            ),
            'Description' => 'Choose one',
        ),
        // the radio field type displays a series of radio button options
        'radioField' => array(
            'FriendlyName' => 'iPay Version ',
            'Type' => 'radio',
            'Options' => ' 2.0 , 3.0',
            'Description' => 'Choose your option!',
        ),
        // the textarea field type allows for multi-line text input
        'Comments' => array(
            'FriendlyName' => 'Comments',
            'Type' => 'textarea',
            'Rows' => '3',
            'Cols' => '60',
            'Description' => 'Freeform multi-line text input field',
        ),
    );

    function __construct()
    {
    //language array - each element key should start with module NAME

    /*
    Now we have the following keys in our $transaction array
    $transaction['merchant_id'],
    $transaction['transaction_id'],
    $transaction['email'],
    $transaction['total'],
    $transaction['merchant_ref'],
    $transaction['memo'],
    $transaction['status'],
    $transaction['date'],
    $transaction['referrer'],
    $transaction['method'],
    $transaction['cur']
    */
if($transaction['merchant_id'] != $merchant_id)die('Invalid merchant');
if($transaction['total'] == 0)die('Invalid total');
    // if($transaction['status'] != 'Approved')die('Failed transaction');
    /*You can do anything you want now with the transaction details or the merchant reference.
    */
if($transaction['status'] == 'Approved'){
if($this->_transactionExists( $_POST['transaction_id']) == false ) {

$this->logActivity(array(
'output' => $transaction,
'result' => PaymentModule::PAYMENT_SUCCESS
));

    // $response['Fee'] = round(($response['Amount'] * $this->configuration['tdr']['value']), 2);

$this->addTransaction(array(
'client_id' => $this->client['id'],
'invoice_id' => $transaction['merchant_ref'],
'description' => $transaction['memo'],
'in' => $transaction['total'],
'fee' => $transaction['total'],
'transaction_id' => $transaction['transaction_id']
));

}

$this->addInfo($this->configuration['success_message']['value']);
Utilities::redirect('?cmd=clientarea');
}else{
    $this->logActivity(array(
        'output' => $transaction,
        'result' => PaymentModule::PAYMENT_FAILURE
    ));
    $this->addInfo($this->configuration['failure_message']['value']);
    Utilities::redirect('?cmd=clientarea');
}
/*}else{
    $this->logActivity(array(
        'output' => "Nothing was returned from VoguePay..Error Error Error",
        'result' => PaymentModule::PAYMENT_FAILURE
    ));
    $this->addInfo($this->configuration['failure_message']['value']);
    Utilities::redirect('?cmd=clientarea');
}

}
}*/
}
function ipay_link($params)
{
    // Gateway Configuration Parameters
    $accountID = $params['accountID'];
    $hashkey = $params['hashkey'];
    $testMode = $params['testMode'];
    // $dropdownField = $params['dropdownField'];
    $radioField = $params['radioField'];
    $Comments = $params['Comments'];

    // Invoice Parameters
    $invoiceId = $params['invoiceid'];
    $orderId  = $invoiceId;

    $description = $params["description"];
    $amount = $params['amount'];
    $currencyCode = $params['currency'];

    // Client Parameters
    $firstname = $params['clientdetails']['firstname'];
    $lastname = $params['clientdetails']['lastname'];
    $email = $params['clientdetails']['email'];
    $address1 = $params['clientdetails']['address1'];
    $address2 = $params['clientdetails']['address2'];
    $city = $params['clientdetails']['city'];
    $state = $params['clientdetails']['state'];
    $postcode = $params['clientdetails']['postcode'];
    $country = $params['clientdetails']['country'];
    $phone = $params['clientdetails']['phonenumber'];

    // System Parameters
    $companyName = $params['companyname'];
    $systemUrl = $params['systemurl'];
    $returnUrl = $params['returnurl'];
    $langPayNow = $params['langpaynow'];
    $moduleDisplayName = $params['name'];
    $moduleName = $params['paymentmethod'];
    $whmcsVersion = $params['whmcsVersion'];
    $pst = array();

    if($radioField == '2.0')
    {
        $url = 'https://www.ipayafrica.com/payments/';


        $pst['live'] = $testMode;
        $pst['mm'] = 1;
        $pst['mb'] = 1;
        $pst['dc'] = 0;
        $pst['cc'] = 1;
        $pst['mer'] = $accountID;
        // $pst['last_name'] = $lastname;
        $pst['oid'] = $invoiceId;
        $pst['inv'] = $invoiceId;
        $pst['ttl'] = $amount;
        $pst['tel'] = $phone;


        $pst['eml'] = $email;
        $pst['vid'] = $accountID;
        $pst['cur'] = $currencyCode;
        $pst['p1'] = '';
        $pst['p2'] = '';
        $pst['p3'] = '';
        $pst['p4'] = '';
        $pst['cbk'] = $systemUrl . '/modules/gateways/callback/' . $moduleName . '.php';
        $pst['cst'] = 1;
        $pst['crl'] = 0;
        $datastring = '';
        foreach ($pst as $key => $value) {
            $datastring .= $value;
        }
        // $datastring = "01101demo20202000.000770221268kiarie@ipayafrica.comdemoKEShttp://cheapdomain.co.ke/modules/gateways/callback/ipay.phpkiarieipayafricacom0";
        // $datastring =  $pst['live'].$pst['mm'].$pst['mb'].$pst['dc'].$pst['cc'].$pst['mer'].$pst['oid'].$pst['inv'].$pst['ttl'].$pst['tel'].$pst['eml'].$pst['vid'].$pst['cur'].$pst['p1'].$pst['p2'].$pst['p3'].$pst['p4'].$pst['cbk'].$pst['cst'].$pst['crl'];
        // $live.$mm.$mb.$dc.$cc.$mer.$oid.$inv.$ttl.$tel.$eml.$vid.$cur.$p1.$p2.$p3.$p4.$cbk.$cst.$crl;
        $hashid = hash_hmac('sha1', $datastring, $hashkey);
        $pst['hsh'] = $hashid;
        // $pst['cbk'] = urlencode($pst['cbk']);

    }
    if($radioField == '3.0')
    {
        $url = 'https://www.ipayafrica.com/payments/v3/ke';

        $pst['live'] = $testMode;
        $pst['inv'] = $invoiceId;
        $pst['oid'] = $orderId;
        $pst['ttl'] = $amount;
        $pst['tel'] = $phone;

        // $pst['currency'] = $currencyCode;
        // $pst['first_name'] = $firstname;
        // $pst['last_name'] = $lastname;
        $pst['eml'] = $email;
        $pst['vid'] = $accountID;
        $pst['curr'] = $currencyCode;
        $pst['p1'] = $orderId;
        $pst['p2'] = $companyName;
        $pst['p3'] = $returnUrl;
        $pst['p4'] = $country;
        $pst['cbk'] = $systemUrl . '/modules/Payment/' . $moduleName . '.php';
        $pst['cst'] = 1;
        $pst['crl'] = 0;


        $datastring =  $pst['live'].$pst['oid'].$pst['inv'].$pst['ttl'].$pst['tel'].$pst['eml'].$pst['vid'].$pst['curr'].$pst['p1'].$pst['p2'].$pst['p3'].$pst['p4'].$pst['cbk'].$pst['cst'].$pst['crl'];
        $hashid = hash_hmac('sha1', $datastring, $hashkey);
        $pst['hsh'] = $hashid;
    }
    //$pst['return_url'] = $returnUrl;

    $htmlOutput = '<form method="post" action="' . $url . '">';
    foreach ($pst as $k => $v) {
        $htmlOutput .= '<input type="hidden" name="' . $k . '" value="' . $v . '" />';
    }
    $htmlOutput .= '<input type="submit" value="' . $langPayNow . '" />';
    $htmlOutput .= '</form>';

    return $htmlOutput;
}