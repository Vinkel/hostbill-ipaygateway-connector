<?php
/**
* Created by PhpStorm.
* User: vinkel
* Date: 1/18/19
* Time: 10:01 AM
*/

class ipay extends PaymentModule
{

    /*
     * const NAME
     * Note: This needs to reflect class name - case sensitive.
     */
    const NAME = 'ipay';

    /*
     * const VER
     * Insert your module version here
     */
    const VER = '3.0';

    /*
     * protected $modname
     * AKA. "Nice name" - you can additionally add this variable - its contents will be displayed as module name after activation
     */
    protected $modname = 'iPay Hostbill';

    /*
     * protected $description
     * If you want, you can add description to module, so its potential users will know what its for.
     */
    protected $description = 'iPay Payment Gateway Module.';

    /*
     * protected $filename
     * This needs to reflect actual filename of module - case sensitive.
     */
    protected $filename = 'class.ipay.php';

    /*
     * protected $supportedCurrencies
     * List of currencies supported by VoguePay.
     */
    protected $supportedCurrencies = array('KES');

    /*
     * protected $configuration
     * Configuration Array
     */

    function ipay_MetaData()
    {
        return array(
            'DisplayName' => 'iPay Payment Gateway',
            'APIVersion' => '3.0',
            'DisableLocalCredtCardInput' => true,
            'TokenisedStorage' => false,
        );
    }

    protected $configuration = array(
        'merchant_id' => array(
            'value' => 'At18NDwy847IEs',
            'type' => 'input'
        ),
        /*'mode' => array(
            'value' => 'demo',
            'type' => 'input'
        ),*/
        'success_message' => array(
            'value' => 'Thank you! Transaction was successful! We have received your payment.',
            'type' => 'input'
        ),
        'failure_message' => array(
            'value' => 'Transaction Failed!',
            'type' => 'input'
        ),

        // the friendly display name for a payment gateway should be
        // defined here for backwards compatibility
        'FriendlyName' => array(
            'Type' => 'System',
            'Value' => 'iPay Payment Gateway',
        ),

        // a text field type allows for single line text input
        'vendorID' => array(
            'FriendlyName' => 'Vendor ID',
            'Type' => 'text',
            'Size' => '25',
            'Default' => '',
            'Description' => 'Enter your vendor ID here',
        ),

        'hashKey' => array(
            'FriendlyName' => 'Hash/Security Key',
            'Type' => 'password',
            'Size' => '25',
            'Default' => '',
            'Description' => 'Enter hash key here',
        ),

        // the yesno field type displays a single checkbox option
        'testMode' => array(
            'FriendlyName' => 'Test Mode',
            'Type' => 'yesno',
            'Description' => 'Tick to enable test mode',
        ),

        // the yesno field type displays a single checkbox option
        'mpesa' => array(
            'FriendlyName' => 'MPesa',
            'Type' => 'yesno',
            'Description' => 'Tick to enable MPesa',
        ),

        // the yesno field type displays a single checkbox option
        'airtel' => array(
            'FriendlyName' => 'Airtel',
            'Type' => 'yesno',
            'Description' => 'Tick to enable Airtel',
        ),

        // the yesno field type displays a single checkbox option
        'equity' => array(
            'FriendlyName' => 'Equitel',
            'Type' => 'yesno',
            'Description' => 'Tick to enable Equitel',
        ),

        // the yesno field type displays a single checkbox option
        'pesalink' => array(
            'FriendlyName' => 'Pesalink',
            'Type' => 'yesno',
            'Description' => 'Tick to enable Pesalink',
        ),

        // the yesno field type displays a single checkbox option
        'creditcard' => array(
            'FriendlyName' => 'Creditcard',
            'Type' => 'yesno',
            'Description' => 'Tick to enable Creditcard',
        ),

        // the yesno field type displays a single checkbox option
        'debitcard' => array(
            'FriendlyName' => 'Debitcard',
            'Type' => 'yesno',
            'Description' => 'Tick to enable Debitcard',
        ),

        // the yesno field type displays a single checkbox option
        'elipa' => array(
            'FriendlyName' => 'eLipa Wallet',
            'Type' => 'yesno',
            'Description' => 'Tick to enable eLipa Wallet',
        ),

    );

    //language array - each element key should start with module NAME
    protected $lang = array(
        'english' => array(
            'ipaymerchant_id' => 'Merchant ID',
            'ipaymode' => 'Mode',
            'ipaysuccess_message' => 'Success Message',
            'ipayfailure_message' => 'Failure Message'
        )
    );

    public function drawForm($params)
    {
        $gatewayaccountid = $this->configuration['merchant_id']['value']; // Your Merchant ID

        # Invoice Variables
        $invoiceid = $this->invoice_id;
        $description = $this->subject;
        $amount = $this->amount;


        # Client Variables
        $name = $this->client['firstname'] . $this->client['lastname'];
        $email = $this->client['email'];
        $address1 = $this->client['address1'];
        $city = $this->client['city'];
        $state = $this->client['state'];
        $postcode = $this->client['postcode'];
        $country = $this->client['country'];
        $phone = $this->client['phonenumber'];

        $merchant_id = $this->configuration['merchant_id']['value']; // Your Merchant ID

        $systemUrl = $params['systemurl'];
        $returnUrl = $params['returnurl'];
        $moduleName = $params['paymentmethod'];
        $langPayNow = $params['langpaynow'];

        $ipay_params = [
            "live" => ($params['testMode'] == "on")? "0" : "1",
            "oid" => $invoiceid,
            "inv" => $invoiceid,
            "ttl" => $amount,
            "tel" => $phone,
            "eml" => $email,
            "vid" => "atlancis",
            "curr" => "KES",
            "p1" => "",
            "p2" => "",
            "p3" => "",
            "p4" => "",
            "cbk" => $systemUrl . "/modules/Payment/ipay/callback/" . $moduleName . ".php",
            "cst" => "1",
            "crl" => "0"
        ];


        $datastring = "";

        foreach ($ipay_params as $key => $value) {
            $datastring .= $value;
        }

        $generatedHash = hash_hmac('sha1',$datastring , "At18NDwy847IEs");

        $ipay_params["hsh"] = $generatedHash;
        $ipay_params["cbk"] = urlencode($ipay_params["cbk"]);
        $ipay_params["lbk"] = urlencode($returnUrl);
        $ipay_params['mpesa'] = ($params['mpesa'] == "on")? "1" : "0";
        $ipay_params['airtel'] = ($params['airtel'] == "on")? "1" : "0";
        $ipay_params['equity'] = ($params['equity'] == "on")? "1" : "0";
        $ipay_params['pesalink'] = ($params['pesalink'] == "on")? "1" : "0";
        $ipay_params['creditcard'] = ($params['creditcard'] == "on")? "1" : "0";
        $ipay_params['debitcard'] = ($params['debitcard'] == "on")? "1" : "0";
        $ipay_params['elipa'] = ($params['elipa'] == "on")? "1" : "0";

        $url = "https://payments.ipayafrica.com/v3/ke";

        $htmlOutput = '<form method="post" action="' . $url . '">';
        foreach ($ipay_params as $k => $v) {
            $htmlOutput .= '<input type="hidden" name="' . $k . '" value="' . urlencode($v) . '" />';
        }
        $htmlOutput .= '<input type="submit" value="' . $langPayNow . '" />';
        $htmlOutput .= '</form>';

        return $htmlOutput;

    }

    function callback($params) {
        $systemUrl = $params['systemurl'];
        $returnUrl = $params['returnurl'];
        $moduleName = $params['paymentmethod'];
        $langPayNow = $params['langpaynow'];

        $ipay_params = [
            "live" => ($params['testMode'] == "on")? "0" : "1",
            "oid" => $params['invoiceid'],
            "inv" => $params['invoiceid'],
            "ttl" => $params['amount'],
            "tel" => $params['clientdetails']['phonenumber'],
            "eml" => $params['clientdetails']['email'],
            "vid" => $params['vendorID'],
            "curr" => $params['currency'],
            "p1" => "",
            "p2" => "",
            "p3" => "",
            "p4" => "",
            "cbk" => $systemUrl . "/modules/Payment/ipay/callback/" . $moduleName . ".php",
            "cst" => "1",
            "crl" => "0"
        ];
    }

}