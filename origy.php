


<?php

if (!defined("WHMCS")) {
    die("This file cannot be accessed directly");
}

function ipay_MetaData()
{
    return array(
        'DisplayName' => 'iPay Payment Gateway',
        'APIVersion' => '3.0',
        'DisableLocalCredtCardInput' => true,
        'TokenisedStorage' => false,
    );
}

function ipay_config()
{
    return array(
        // the friendly display name for a payment gateway should be
        // defined here for backwards compatibility
        'FriendlyName' => array(
            'Type' => 'System',
            'Value' => 'iPay Payment Gateway',
        ),

        // a text field type allows for single line text input
        'vendorID' => array(
            'FriendlyName' => 'Vendor ID',
            'Type' => 'text',
            'Size' => '25',
            'Default' => '',
            'Description' => 'Enter your vendor ID here',
        ),

        'hashKey' => array(
            'FriendlyName' => 'Hash/Security Key',
            'Type' => 'password',
            'Size' => '25',
            'Default' => '',
            'Description' => 'Enter hash key here',
        ),

        // the yesno field type displays a single checkbox option
        'testMode' => array(
            'FriendlyName' => 'Test Mode',
            'Type' => 'yesno',
            'Description' => 'Tick to enable test mode',
        ),

        // the yesno field type displays a single checkbox option
        'mpesa' => array(
            'FriendlyName' => 'MPesa',
            'Type' => 'yesno',
            'Description' => 'Tick to enable MPesa',
        ),

        // the yesno field type displays a single checkbox option
        'airtel' => array(
            'FriendlyName' => 'Airtel',
            'Type' => 'yesno',
            'Description' => 'Tick to enable Airtel',
        ),

        // the yesno field type displays a single checkbox option
        'equity' => array(
            'FriendlyName' => 'Equitel',
            'Type' => 'yesno',
            'Description' => 'Tick to enable Equitel',
        ),

        // the yesno field type displays a single checkbox option
        'pesalink' => array(
            'FriendlyName' => 'Pesalink',
            'Type' => 'yesno',
            'Description' => 'Tick to enable Pesalink',
        ),

        // the yesno field type displays a single checkbox option
        'creditcard' => array(
            'FriendlyName' => 'Creditcard',
            'Type' => 'yesno',
            'Description' => 'Tick to enable Creditcard',
        ),

        // the yesno field type displays a single checkbox option
        'debitcard' => array(
            'FriendlyName' => 'Debitcard',
            'Type' => 'yesno',
            'Description' => 'Tick to enable Debitcard',
        ),

        // the yesno field type displays a single checkbox option
        'elipa' => array(
            'FriendlyName' => 'eLipa Wallet',
            'Type' => 'yesno',
            'Description' => 'Tick to enable eLipa Wallet',
        ),

    );
}

function ipay_link($params){

    $systemUrl = $params['systemurl'];
    $returnUrl = $params['returnurl'];
    $moduleName = $params['paymentmethod'];
    $langPayNow = $params['langpaynow'];

    $ipay_params = [
        "live" => ($params['testMode'] == "on")? "0" : "1",
        "oid" => $params['invoiceid'],
        "inv" => $params['invoiceid'],
        "ttl" => $params['amount'],
        "tel" => $params['clientdetails']['phonenumber'],
        "eml" => $params['clientdetails']['email'],
        "vid" => $params['vendorID'],
        "curr" => $params['currency'],
        "p1" => "",
        "p2" => "",
        "p3" => "",
        "p4" => "",
        "cbk" => $systemUrl . "/modules/gateways/callback/" . $moduleName . ".php",
        "cst" => "1",
        "crl" => "0"
    ];


    $datastring = "";

    foreach ($ipay_params as $key => $value) {
        $datastring .= $value;
    }

    $generatedHash = hash_hmac('sha1',$datastring , "At18NDwy847IEs");

    $ipay_params["hsh"] = $generatedHash;
    $ipay_params["cbk"] = urlencode($ipay_params["cbk"]);
    $ipay_params["lbk"] = urlencode($returnUrl);
    $ipay_params['mpesa'] = ($params['mpesa'] == "on")? "1" : "0";
    $ipay_params['airtel'] = ($params['airtel'] == "on")? "1" : "0";
    $ipay_params['equity'] = ($params['equity'] == "on")? "1" : "0";
    $ipay_params['pesalink'] = ($params['pesalink'] == "on")? "1" : "0";
    $ipay_params['creditcard'] = ($params['creditcard'] == "on")? "1" : "0";
    $ipay_params['debitcard'] = ($params['debitcard'] == "on")? "1" : "0";
    $ipay_params['elipa'] = ($params['elipa'] == "on")? "1" : "0";

    $url = "https://payments.ipayafrica.com/v3/ke";

    $htmlOutput = '<form method="post" action="' . $url . '">';
    foreach ($ipay_params as $k => $v) {
        $htmlOutput .= '<input type="hidden" name="' . $k . '" value="' . urlencode($v) . '" />';
    }
    $htmlOutput .= '<input type="submit" value="' . $langPayNow . '" />';
    $htmlOutput .= '</form>';

    return $htmlOutput;
}

?>